function [pc,points] = linestabilizer(im,lines,range,outlier_limit,blur_sigma)
% To be used for detecting the horizon.
% Outputs the polynomial coefficients "pc" from p1x + p2, confidence (lower
% is better) and detected points "points"
% Input is the image containing the horizon and the number of sampling
% lines

% Sample the frame with specified selection of columns
columns = ceil(linspace(range(1),size(im,2)-range(2),lines));
reduced_image = mean(im(:,columns,:),3);

% Detect the edges from the sampled image
edges = ut_edge(reduced_image,'c','s',blur_sigma);

% Get the points on the horizion
points=[];
for i = 1:length(columns)
    [~,index] = max(edges(:,i,:));
    points = cat(1,points,[columns(i) ceil(mean(index))]);
end

% Checking for outliers
k=0;
for j=1:length(points)
   if abs(points(j-k,2)-mean(points(:,2)))>outlier_limit
      points(j-k,:)=[];
      k=k+1;
   end
end

% Fit a line through the horizon points
[pc,~] = polyfit(points(:,1),points(:,2),1);

end

