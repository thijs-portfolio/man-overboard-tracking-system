function [state_estimate,P,K] = kalman_update(measurement,state_estimate,Ez,Ex,P,a,A,B,C)
% This function performs a kalman update step for the man overboard
% tracking algorithm. It uses the variables returned by the
% initialize_kalman function and outputs the updated state and updated
% error.

    % Calculate Kalman Gain
    K = P * C' * inv(C * P * C' + Ez);
    
    % Perform Kalman measurement update if the detection was reliable
    if ~isempty(measurement)
        
        %Measurement update:
        state_estimate = state_estimate + K * ([measurement(1);measurement(2)] - C * state_estimate);
        
    end
   
    % Update covariance estimation
    P = (eye(4) - K * C) * P;

end

