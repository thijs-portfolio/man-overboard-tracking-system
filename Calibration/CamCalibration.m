% Camera Calibration for 20mm and 50mm lens
%-------------------------------------------------------

% Define images from 20mm to process
imageFileNames = {'D:\Dropbox\Image processing\Project\project 2\MV_f2.8_100\MV_f2.8_100_1.jpg',...
    'D:\Dropbox\Image processing\Project\project 2\MV_f2.8_100\MV_f2.8_100_24.jpg',...
    'D:\Dropbox\Image processing\Project\project 2\MV_f2.8_100\MV_f2.8_100_47.jpg',...
    'D:\Dropbox\Image processing\Project\project 2\MV_f2.8_100\MV_f2.8_100_116.jpg',...
    'D:\Dropbox\Image processing\Project\project 2\MV_f2.8_100\MV_f2.8_100_139.jpg',...
    'D:\Dropbox\Image processing\Project\project 2\MV_f2.8_100\MV_f2.8_100_162.jpg',...
    'D:\Dropbox\Image processing\Project\project 2\MV_f2.8_100\MV_f2.8_100_208.jpg',...
    'D:\Dropbox\Image processing\Project\project 2\MV_f2.8_100\MV_f2.8_100_231.jpg',...
    'D:\Dropbox\Image processing\Project\project 2\MV_f2.8_100\MV_f2.8_100_254.jpg',...
    'D:\Dropbox\Image processing\Project\project 2\MV_f2.8_100\MV_f2.8_100_369.jpg',...
    'D:\Dropbox\Image processing\Project\project 2\MV_f2.8_100\MV_f2.8_100_415.jpg',...
    'D:\Dropbox\Image processing\Project\project 2\MV_f2.8_100\MV_f2.8_100_438.jpg',...
    'D:\Dropbox\Image processing\Project\project 2\MV_f2.8_100\MV_f2.8_100_461.jpg',...
    'D:\Dropbox\Image processing\Project\project 2\MV_f2.8_100\MV_f2.8_100_484.jpg',...
    'D:\Dropbox\Image processing\Project\project 2\MV_f2.8_100\MV_f2.8_100_507.jpg',...
    'D:\Dropbox\Image processing\Project\project 2\MV_f2.8_100\MV_f2.8_100_553.jpg',...
    };

% Detect checkerboards in images
[imagePoints, boardSize, imagesUsed] = detectCheckerboardPoints(imageFileNames);
imageFileNames = imageFileNames(imagesUsed);

% Generate world coordinates of the corners of the squares
squareSize = 50;  % in units of 'mm'
worldPoints = generateCheckerboardPoints(boardSize, squareSize);

% Calibrate the camera with 20mm lens
[cameraParams20, imagesUsed20, estimationErrors20] = estimateCameraParameters(imagePoints, worldPoints, ...
    'EstimateSkew', false, 'EstimateTangentialDistortion', false, ...
    'NumRadialDistortionCoefficients', 2, 'WorldUnits', 'mm');

% Define images from 50mm to process
imageFileNames = {'D:\Dropbox\Image processing\Project\project 2\MV_tele_2.5_125\MV_2.5_125_1.jpg',...
    'D:\Dropbox\Image processing\Project\project 2\MV_tele_2.5_125\MV_2.5_125_19.jpg',...
    'D:\Dropbox\Image processing\Project\project 2\MV_tele_2.5_125\MV_2.5_125_109.jpg',...
    'D:\Dropbox\Image processing\Project\project 2\MV_tele_2.5_125\MV_2.5_125_127.jpg',...
    'D:\Dropbox\Image processing\Project\project 2\MV_tele_2.5_125\MV_2.5_125_145.jpg',...
    'D:\Dropbox\Image processing\Project\project 2\MV_tele_2.5_125\MV_2.5_125_397.jpg',...
    'D:\Dropbox\Image processing\Project\project 2\MV_tele_2.5_125\MV_2.5_125_415.jpg',...
    'D:\Dropbox\Image processing\Project\project 2\MV_tele_2.5_125\MV_2.5_125_487.jpg',...
    'D:\Dropbox\Image processing\Project\project 2\MV_tele_2.5_125\MV_2.5_125_613.jpg',...
    'D:\Dropbox\Image processing\Project\project 2\MV_tele_2.5_125\MV_2.5_125_649.jpg',...
    'D:\Dropbox\Image processing\Project\project 2\MV_tele_2.5_125\MV_2.5_125_703.jpg',...
    'D:\Dropbox\Image processing\Project\project 2\MV_tele_2.5_125\MV_2.5_125_811.jpg',...
    'D:\Dropbox\Image processing\Project\project 2\MV_tele_2.5_125\MV_2.5_125_829.jpg',...
    'D:\Dropbox\Image processing\Project\project 2\MV_tele_2.5_125\MV_2.5_125_865.jpg',...
    };

% Detect checkerboards in images
[imagePoints, boardSize, imagesUsed] = detectCheckerboardPoints(imageFileNames);
imageFileNames = imageFileNames(imagesUsed);

% Generate world coordinates of the corners of the squares
worldPoints = generateCheckerboardPoints(boardSize, squareSize);

% Calibrate the camera with 50mm lens
[cameraParams50, imagesUsed50, estimationErrors50] = estimateCameraParameters(imagePoints, worldPoints, ...
    'EstimateSkew', false, 'EstimateTangentialDistortion', false, ...
    'NumRadialDistortionCoefficients', 2, 'WorldUnits', 'mm');
