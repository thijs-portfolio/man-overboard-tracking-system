clear all;
close all;
clc

% These are parameters that can be edited for each video
filename = 'MAH01462';
skip_frame = 6;
correlation_threshold = 0.84;
intensity_threshold = 0.5;
model_error = 0.1;


addpath('Functions','Calibration','Template dictionary','Videos')

% Load the video file and template dictionary
h_status = waitbar(0.1,'Reading video file...');
hVideoSrc = vision.VideoFileReader([filename,'.MP4']);
waitbar(0.3,h_status,'Initializing...');
load(['templates_',filename,'.mat'])
template_set = struct2cell(template_set);

% Skip to the first frame where the buoy is visible
for i=1:skip_frame
    img = step(hVideoSrc);
    waitbar(0.5+i/(10*skip_frame) ,h_status,['Initializing... (',num2str(i),'/',num2str(skip_frame),')']);
end
img = step(hVideoSrc);
img_old = img;

waitbar(0.8,h_status,['Initializing...']);


% Initialize line stabilization
[pc,points] = linestabilizer(img,100,[10 10],200,8);
hor_ref = pc(2);
a = -tan(pc(1));
T = [cos(a) sin(a) 0; -sin(a) cos(a) 0; 0 hor_ref-pc(2) 1];
tform = maketform('affine', T);
img = imtransform(img,tform,'XData',[1 size(img,2)],'YData',[1 size(img,1)]);

%Select first buoy location
figure(1)
h=imshow(img);
hold on
waitfor(msgbox('Zoom in on the buoy and press OK'));
[xpeak,ypeak] = ginput(1); % XY coordinates are flipped in row column notation

xpeak = round(xpeak);    % Round for row column indices
ypeak = round(ypeak);
close(figure(1))

ymean = ypeak;          % Ymean keeps track of last y coordinates


% Initialize kalman filter:
[ state_estimate,Ez,Ex,P,a,A,B,C ] = initialize_kalman([xpeak ypeak],0,1,[250 200],model_error);

% Initialize the search area size
xband = 60;
yband = 15;

close(h_status);



figure(1)
h_im=imshow(img);
hold on;
for pm = 1:length(template_set)
    p_measurement(pm) = plot(xpeak,ypeak,'oy','MarkerSize',10); % plot marker for each dictionary entry
end
p_kalman = plot(xpeak,ypeak,'or'); % Plot Kalman tracker point
hold off;

%Initialize loop variables
x=xpeak;
y=ypeak;
init = 1;
prevwidth = 0;
psizes=[];
dis =[];
ctresh = [];
framecount = 0;

while ~isDone(hVideoSrc)
    
    img = step(hVideoSrc); % Get the next frame
    
    
    % Stabilization
    [pc,points] = linestabilizer(img,50,[10 10],200,8);    
    a = -tan(pc(1)); % Get horizon angle
    T = [cos(a) sin(a) 0; -sin(a) cos(a) 0; 0 hor_ref-pc(2) 1]; % Calculate transformation matrix
    tform = maketform('affine', T);
    img = imtransform(img,tform,'XData',[1 size(img,2)],'YData',[1 size(img,1)]); % Apply affine transformation
    
    % Kalman prediction
    [state_estimate,P] = kalman_prediction(state_estimate,Ez,Ex,P,a,A,B,C);
    
    % Define new search area
    search_area = img(round(state_estimate(2)-yband):round(state_estimate(2)+yband),round(state_estimate(1)-xband):round(state_estimate(1)+xband),:);
    
    % Do a measurement step for each template dictionary entry
    for tem = 1:length(template_set)
        
        [xtemp,ytemp,corval] = track2(search_area, template_set{tem},intensity_threshold,correlation_threshold);
        
        if ~isempty(xtemp)  % If there is an acceptable measurement
            
            x = xtemp-xband + state_estimate(1);    % Transform from search area to video frame
            y = ytemp-yband + state_estimate(2);
            
            if abs((y-hor_ref)-(mean(ymean)-hor_ref))>10 % Reject the measurement if the change in y 
                x = [];                                  % with respect to the horizon was too big
                y = [];
            else
                
                %Kalman filter update:
                [state_estimate,P,K] = kalman_update([x y],state_estimate,Ez,Ex,P,a,A,B,C);
                
                % Update plot
                set(p_measurement(tem),'XData',x,'YData',y,'Visible','on');
                
                % Calculate distance
                [dis,psizes] = distance(img,round(x),round(y),'w',20,30,psizes);
                
            end
        else
            x=[];
            y=[];
            set(p_measurement(tem),'Visible','off','XData',x,'YData',y)
        end
        
    end
    
    if(length(ymean)<50) % Keep track of the last 50 y coordinates
        ymean = [ymean;state_estimate(2)];
    else
        ymean = [ymean;state_estimate(2)];
        ymean(end) = [];
    end
    
    
    % Update distance estimation information
    if(~isempty(dis))
        disp(['Distance estimation between ',num2str(dis(1)),' m and ',num2str(dis(2)),' m'])
    end

    % Update plot
    img_old = img;
    img(1:(yband+0.5)*12,1:(xband+0.5)*12,:) = imresize(search_area,6); % Plot the enlarged search area in the original frame
    set(h_im,'CData',img)
    set(p_kalman,'XData',state_estimate(1),'YData',state_estimate(2));
    
    if isempty(x)
        xband = xband + 2; % Grow the search area if there was no measurement
    else
        xband = 50; % Reset search area size if a measurement was present
    end
    
    drawnow;
    
    framecount = framecount +1;
    disp(['Frame number: ',num2str(framecount)])
    
end

disp('Done')